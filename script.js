    var $policko = $(".txtb");
	var $telo = $(".telo");
	var $navigace = $(".navigace");
	var $nadpis = $(".nadpis");
	var $ukol = $(".ukol");
	var $mensiNadpis = $(".mensiNadpis");
	var $tlacitko = $(".tlacitko");
	var $plus = $(".plus");


$( document ).ready(function() {
    $(".txtb").on("keyup",function(e){
			if(e.keyCode == 13 && $.trim($policko.val()) != "")
			{
				var ukol = $("<div class='ukol'></div>").text($(".txtb").val());
				var smazat = $("<i class='fas fa-trash-alt'></i>").click(function() {
					var p = $(this).parent();
					p.fadeOut('50', function() {
						p.remove();
					});
				});
				var fajvka = $("<i class='fas fa-check'></i>").click(function() {
					var p = $(this).parent();
					p.fadeOut('50', function() {
						$(".hotove").append(p);
						p.fadeIn();
					});
					$(this).remove();
				});

				ukol.append(smazat,fajvka);
				$(".nehotove").append(ukol);
				//vycisteni pole
				$(".txtb").val("");
				$($policko).focus();
			}else if(e.keyCode == 13 && $.trim($policko.val()) == ""){
				alert("Je potřeba vyplnit název/popis úkolu.");
				$($policko).focus();
			}
		})

	$tlacitko.on('click',() => {
		$telo.toggleClass('darktelo');
		$navigace.toggleClass('dark');
		$tlacitko.toggleClass('dark');
		$ukol.toggleClass('dark');
		alert("Styl byl změněn.");
	});	


	$policko.focusin(function(){
		$plus.animate({
			width: '100px'});		
	});

	$policko.focusout(function(){
		$plus.animate({
			width: '50px'});	
	});


	var ukol = $("<div class='ukol'></div>").text($(".txtb").val());
    $plus.on("click",function(){
	if($.trim($policko.val()) != "")
	{
		var ukol = $("<div class='ukol'></div>").text($(".txtb").val());
				var smazat = $("<i class='fas fa-trash-alt'></i>").click(function() {
					var p = $(this).parent();
					p.fadeOut('50', function() {
						p.remove();
					});
				});
				var fajvka = $("<i class='fas fa-check'></i>").click(function() {
					var p = $(this).parent();
					p.fadeOut('50', function() {
						$(".hotove").append(p);
						p.fadeIn();
					});
					$(this).remove();
				});

				ukol.append(smazat,fajvka);
				$(".nehotove").append(ukol);
				//vycisteni pole
				$(".txtb").val("");
				$($policko).focus();
	}else if($.trim($policko.val()) == ""){
		alert("Je potřeba vyplnit název/popis úkolu.");
		$($policko).focus();
	}

    })

});












		